/* The async behaviour of the following code follows from the fact that
setTimeout() queues the code to be executed once the stack is finished.
So, all processing happens in a single thread. setTimeout() method can be
used with "0" timeout value.
*/

function no_ftl(){
    console.log('You will wait three stupid seconds anyway.');
}

function asyncno_ftl(){
    // Liked this one!
    setTimeout(no_ftl, 3000);
}

function ftl(){
    console.log('Hmm, faster than light.');
}

// Function calls

ftl();
asyncno_ftl();
asyncno_ftl();
ftl();
asyncno_ftl();
ftl();
ftl();
ftl();