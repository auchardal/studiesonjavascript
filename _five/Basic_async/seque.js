function no_ftl(){
    var current_time = new Date().getTime();
    while(new Date().getTime() < current_time + 3000){
        // Do nothing and wait three seconds.
        // Is there a Pythonic time.sleep() analogy in JS?
    }
    console.log('You waited three stupid seconds.');
}

function ftl(){
    console.log('Hmm, faster than light.');
}

// Function calls

ftl();
no_ftl();
no_ftl();
ftl();
no_ftl();
ftl();
ftl();
ftl();