/* the race() function takes the result of the promise that rejects or
resolves the fastest given an array of promises.

A very cool method that can be utilise to limit execution times or even
benchmarking "methods" that has different algorthms for the same
functionality. */

var first_promise = new Promise(function(resolve, reject) {
    setTimeout(function(){
        resolve('I am slover.');
    }, 5000);
});

var second_promise = new Promise(function(resolve, reject) {
    setTimeout(function(){
        resolve('I am the fastest.');
    }, 1000);
});

Promise.race([first_promise, second_promise]).then(function(_data) {
    console.log(_data);
}).catch(function(error) {
    console.log(error);
});

/* Two important notes from Mozilla:

    - If the iterable passed is empty, the promise returned will be 
    forever pending.
    - If the iterable contains one or more non-promise value and/or 
    an already settled promise, then `race` method will resolve to the
    first of these values found in the iterable.
    
*/