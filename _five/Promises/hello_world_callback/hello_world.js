/* According to Mozilla, see here,

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

a Promise objects represents the eventual completion or failure of an
asynchronous operation and its resulting value.

I found the concept a bit vogue, but I am hoping it will be clear after 
implementing sufficient examples. */

/* In what follows, I define two promises. First one resolves to the value
, which is the word "Hello" after half a second. Second promise is created
by using a 'return' statement to transform the held value the within the
then()' callback. Finally, we make another callback to the second promise 
to log "Hello, world!". */

var first_promise = new Promise(function(resolve, reject){
    setTimeout(function() {
        resolve('Hello');
    }, 500);
});

var second_promise = first_promise.then(function(data) {
    return data + ', world!';
});

second_promise.then(function(data) {
    console.log(data);
});