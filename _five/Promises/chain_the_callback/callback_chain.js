/* Let us create a promise and via using callback chain,
let us change the value(s) to resolve.

I do not know, yet, that how practical this chains are or
that is how efficient they are (outside of the async programming). */

var promise = Promise.resolve([2, 3, 5, 8, 13, 21]);

promise.then(function(_data) {
    return _data.map(x => x * x - x);

}).then(function(_data) {
    console.log(_data); // Should log the results from the first callback.
    return _data.filter(x => x < 100);
    
}).then(function(_data) {
    console.log(_data);
    return _data;

}).catch(function(error) {
    console.log(error);
})

/* Many other functions may added to this chain.
I love the way how this works but still unclear about its practical value.
This hopefully may change.*/

/* Ok regarding my last comment: this chains are indeed halpfull if we wanna
sequence async operations as one can return a promise within a then()
callback; this is cool. */