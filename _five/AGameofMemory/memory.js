/* This is the JavaScript code file for the basic memory game.
The code can be made more complex (which I am planning to do for a
more complex game), however I will try to keep it minimal. */

var clickedCellList = new Array();
var ready = true; // To handle click events
var numCompleted = 0;

function clickedCells(_cell) {
    /* A function to push clicked cells to a list.
    This list can be useful in certain situations, while at first seems
    unnecessary. */
    clickedCellList.push(_cell);
}

function makeVisible(_cell) {
    /* The function that makes visible the content of a cell
    after it is clicked by the end user. It also changes the
    background color and the ``.clicked`` attribute. */
    _cell.style.backgroundColor = 'azure';
    _cell.innerHTML = _cell.value;
    _cell.clicked = true;
}

function makeInvisible(_cell) {
    /* If the cell values do not match, make the content invisible and
    the cell unclicked. */
    _cell.style.backgroundColor = 'lightskyblue';
    _cell.innerHTML = '';
    _cell.clicked = false;
}

function paired(_cell) {
    /* If we correctly found a pair, they stay visible with different
    background color. */
    numCompleted++;
    _cell.paired = true;
    _cell.style.backgroundColor = 'lightgray';
}

function _permuteList(_list) {
    /* Note that "var" declares variables within the function scope, whereas
    "let" is used to declare variables within the immediate enclosing block.*/

    /*To permute a given list, I tried to implement my way. I ended up with
    an algorithm which turned out to be a crude version of Durstenfeld shuffle
    (optimized Fisher-Yates). Then, ES6 magic happened. See

    https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array

    for further info.*/

    for (let i = _list.length-1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        // Next line is fastinating :)
        [_list[i], _list[j]] = [_list[j], _list[i]];
    }
    return _list;
}

function randomPairGenerator() {
    /* The game is basic. We have 16 cells that shall be occupied with 8 pairs
    of randomly selected numbers. That is we shall select 8 numbers from
    0-100, add them to a list twice and scrumble the list. */

    var randomPairs = new Array();
    const numList = [...Array(101).keys()]; // ES6 seems closer to Python.
    for (var i = 0; i < 15; i+=2) {
        var randomNumber = numList[Math.floor(Math.random()*numList.length)];
        // We need some type of validation here to ensure we do not append
        // the same pair of numbers twice.
        // I am not sure what is the max lenght of an Array for which the
        // following check becomes a performance issue, as the search has to
        // start from index zero at every iteration.
        if (randomPairs.includes(randomNumber) == false) {
            randomPairs[i] = randomNumber;
            randomPairs[i+1] = randomPairs[i];
        }
        else {
            // In a for loop, the increment expression is first evaluated. Thus,
            // we decrement by 2 if the above check fails to make sure we have
            // exactly 16 elements (8 pairs of numbers) in the list.
            i=i-2;
        }
    }
    console.log(randomPairs);
    return _permuteList(randomPairs);
}

function checkPair() {
    if (clickedCellList[0].value == clickedCellList[1].value) {

        paired(clickedCellList[0]);
        paired(clickedCellList[1]);

        clickedCellList = [];
    }
    else {
        /* We need to implement a timeout here, otherwise
        it is impossible to see the value in the cell
        before it is hidden again. This function in fact can be used to 
        determine, by user, how hard the game is gonna be.*/
        setTimeout(function(){
            ready = false;

            makeInvisible(clickedCellList[0]);
            makeInvisible(clickedCellList[1]);

            clickedCellList = [];

            ready = true;
        }, 750)
    }
}

// This can be a class?
function cellManipulator() {
    /* This is the main function where we add event listeners and
    attributes to cells. Here, how JS works a bit weird, mainly, the ability
    of declaring (dot) attributes to an "Object" without the need of a class
    construction. */
    var matrix = document.getElementsByTagName('td');
    var pairs = randomPairGenerator();

    for (var i = 0; i < matrix.length; i++) {
        var cell = matrix[i];
        cell.paired = false;
        cell.clicked = false;
        cell.value = pairs[i];

        // Add event listeners

        cell.addEventListener('mouseenter', function() {
            if (this.paired == false && this.clicked == false){
                this.style.background = 'lightyellow';
            }
        });

        cell.addEventListener('mouseleave', function() {
            if (this.paired == false && this.clicked == false){
                this.style.background = 'lightskyblue';
            }
        });

        cell.addEventListener('click', function() {
            if (ready == false) {
                return;
            }
            if (this.paired == false && this.clicked == false){
                clickedCells(this);
                makeVisible(this);
            }
            if (clickedCellList.length == 2) {
                checkPair();
            }
        });
    }

    document.getElementById('restart').addEventListener('click', function() {
        location.reload();
    });
}

cellManipulator();
