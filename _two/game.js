/*
Basic guessing game. We choose a random number.
User tries to guess this number with limited amount
of tries.
*/

var number = Math.floor(Math.random() * 100);
var guess;
var limit = 5;
var won = false;
var guesses = new Array();

for (i = 1; i <= limit; i++){

    do{
        guess = parseInt(prompt('Guess a number'));
    } while (isNaN(guess) || isPreviousGuess(guess));

    if (guess == number){
        document.write('Correct! You won.');
        won = true;
        break;
    }
    else{
        guesses.push(guess);
        alert(`Incorrect! \nTries remaining: ${(limit -i)}`);
    }
}

if (!won){
    document.write('Sorry, you ran out of tries. Game over.'
               +` \nThe correct answer was ${number}.`);
}

function isPreviousGuess(){
    for (i = 0; i<guesses.length; i++){
        if (guesses[i] == guess){
            alert('This guess has been made before.')
            return true;
        }
    }
    return false;
}
console.log(guesses)