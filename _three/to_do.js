// Add variables to manipulate DOM

var addButton = document.getElementById('add');
var taskInput = document.getElementById('task');
var taskList = document.getElementById('taskList');
var clearButton = document.getElementById('clear');

// Observe that I am using a first-class functions.
addButton.addEventListener('click', function(){
    var task = taskInput.value;
    // We get rid of all the trailing and leading white spaces via .trim()
    if (task.trim()) {
       var newItem = document.createElement('li');
       var taskText = document.createTextNode(task);
       newItem.appendChild(taskText);
       taskInput.value = '';
       var removeButton = document.createElement('button');
       removeButton.innerHTML = 'Done';
       removeButton.className = 'remove';
       removeButton.addEventListener('click', removeTask);
       newItem.appendChild(removeButton);
       taskList.appendChild(newItem);
    }
    else {
        alert('Please provide a valid entry.')
        console.log('Tasks cannot be empty strings.')
    }
})

clearButton.addEventListener('click', function(){
    taskList.innerHTML = '';
})

// Helper function to remove a task from the list.

function removeTask(e) {
    var taskItem = e.target.parentElement;
    taskList.removeChild(taskItem);
}