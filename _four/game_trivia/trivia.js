// Global variables

var questionID;
var question;
var choiceA, choiceB, choiceC, choiceD;
var questionPool;
var lenQuestionPool;
var questionInfo;
var currentQuestionID = 0;
var selectedChoice;
var score = 0;
var gameScore = new Array();

// Pointers to DOM

var dQuiz = document.getElementById('quiz');
var dQuizStatus = document.getElementById('quizStatus');
var dQuestion = document.getElementById('question');
var dChoiceA = document.getElementById('choiceA');
var dChoiceB = document.getElementById('choiceB');
var dChoiceC = document.getElementById('choiceC');
var dChoiceD = document.getElementById('choiceD');
var dChoices = document.getElementsByName('choices');

// Set default questions

var defaultQPool = [
    {
        question: "1, 1, 2, 3, 5, ?",
        choiceA: "8",
        choiceB: "22",
        choiceC: "33",
        choiceD: "1",
        correct: "A"
    },
    {
        question: "What does JSON stands for?",
        choiceA: "Java Syntax Object Nature",
        choiceB: "J Semantic Object Notation",
        choiceC: "Jackson Style is ON",
        choiceD: "JavaScript Object Notation",
        correct: "D"
    },
    {
        question: "What is absolute zero temperature?",
        choiceA: "It is the absolute value of the freezing point of water.",
        choiceB: "There is no such a thing.",
        choiceC: "It is 0 Kelvin.",
        choiceD: "It is the temperature of an electron.",
        correct: "C"
    },
    {
        question: "3! = ?",
        choiceA: "6",
        choiceB: "1",
        choiceC: "22",
        choiceD: "4",
        correct: "A"
    },
    {
        question: "What is the total number of chromosomes in a human cell?",
        choiceA: "1",
        choiceB: "23",
        choiceC: "46",
        choiceD: "3.14",
        correct: "C"
    }];

function setQuestions(){
    questionPool = defaultQPool;
    lenQuestionPool = questionPool.length;
}

function getQuestionInfo(){
    question = questionPool[currentQuestionID].question;
    questionInfo = questionPool[currentQuestionID];
    choiceA = questionInfo.choiceA;
    choiceB = questionInfo.choiceB;
    choiceC = questionInfo.choiceC;
    choiceD = questionInfo.choiceD;
    correct = questionInfo.correct;
}

function displayQuiz(){
    questionID = currentQuestionID + 1;
    dQuizStatus.innerHTML = `Question ${questionID} of ${lenQuestionPool}`;

    getQuestionInfo();

    dQuestion.innerHTML = question;
    dChoiceA.innerHTML = choiceA;
    dChoiceB.innerHTML = choiceB;
    dChoiceC.innerHTML = choiceC;
    dChoiceD.innerHTML = choiceD;
}

// We implement a function which displays the results of the Quiz.

function gameOver(){
    dQuiz.innerHTML = `<h2>Score: ${score} </h2>`
    for (var i = 0; i < gameScore.length; i++){
        var results = document.createElement('p'); // Push out of the loop?
        if (gameScore[i] == 0){
            results.innerHTML = `Question ${i+1} is incorrect.`
            results.style.color = 'red';
        }
        else {
            results.innerHTML = `Question ${i+1} is correct.`
            results.style.color = 'green';
        }
        dQuiz.appendChild(results);
    }
    document.getElementById("options").style.display = "block";
}

// Let us get the user choice and clear the radio
// button for the next choice. To this end we create another
// function that shall "check" if the question is answered.

// TODO: Implement "Pass" option, if user does not know the answer to
// the question.

function getChoice(){
    for (var i = 0, len = dChoices.length; i < len; i++){
        if (dChoices[i].checked){
            selectedChoice = dChoices.value;
            dChoices[i].checked = false;
        }
    }
    return false;
}

// Now, let us grade the user answer.

function gradeAnswer(){
    if (getChoice()){
        if (selectedChoice == questionPool[currentQuestionID].correct){
            score++;
            gameScore[currentQuestionID] = 1;
        }
        else {
            gameScore[currentQuestionID] = 0;
        }
        if (currentQuestionID == questionPool.length-1){
            gameOver();
        }
        else {
            currentQuestionID++;
            displayQuiz();
        }
    }
}

// Function calls

setQuestions()
displayQuiz()
document.getElementById("submit").onclick = gradeAnswer;