var numberInput = document.getElementById('number');
var validatationMessage = document.getElementById('validationMessage');
var nanMessage = document.getElementById('nanMessage');
// Validate that the input number is in between 1-100.

function verify(){
    if (numberInput.value < 1 || numberInput.value > 100){
        validatationMessage.style.display = 'block';
        return false;
    }
    // Validate that input is a numeric type.
    else if  (isNaN(numberInput.value)){
        nanMessage.style.display = 'block';
        return false;
    }
    else {
        validatationMessage.style.display = 'none';
        return true;
    }
}

numberInput.onchange = function(){
    if (isNaN(numberInput.value)){
        nanMessage.style.display = 'block';
        return false;
    }
    else {
        nanMessage.style.display = 'none';
        return true;
    }
}