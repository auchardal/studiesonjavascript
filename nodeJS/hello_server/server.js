/* A basic server code. Here, we shall listen the port `3000`, and provide
a basic response. Nothing special really. */

/* Ok, I cannot use ES6 style `import`, as for static use, I need a 
"type = module" statement. There is a dynamic "function-like" `import()` 
which can be used, but requires to change the code which does not worth it
at the moment. So `require` should do. */

var http = require('http');

// Port to listen
var port = 3000;

var requestHandler = function(request, response) {
    console.log(`We have a request from ${request.url}`);
    response.end('Hellloooo!!');
}

var server = http.createServer(requestHandler);

server.listen(port, function() {
    console.log(`Listening on port ${port}`);
});