/* This is the basic hello code for using "express" module to create
a HTTP server and the respond route. */

var express = require('express');
var application = express();
var port = 3000;

application.get(`/`, function(request, response){
    response.send('Hi there :)');
});

application.listen(port, function() {
    console.log(`Listening on port ${port}.`);
});

