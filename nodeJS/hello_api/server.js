/* This will be a basic web-app; to `get` and `post` strings. We will use
`express` module as the dependency. 

We shall use a simple JSON object within the code as we are not connected
to a database yet.*/ 

// How easy it is to define, fundamentall Objects, in JS! Awesome.

var random_sentences = [
    {
        id: 1,
        sentence: "There is no God.",
        author: "Who cares?",
        year: 2019
    },
    {
        id: 2,
        sentence: "Yes, I am ready.",
        author: "No one knows!",
        year: 1001
    },
    {
        id: 3,
        sentence: "All is well.",
        author: "Amir Khan.",
        year: 2009
    }
];

var express = require('express');
var app = express();

app.get('/', function(req, res) {
    res.send("The get request received at '/' ");
});

app.get('/sentences', function(req, res) {
    res.json(random_sentences); // ah, cool.
});

app.listen(3000, function() {
    console.log('Listening on Port 3000.');
});