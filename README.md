# StudiesOnJavaScript

A JavaScript repository that holds some exploratory code. The code can be very basic or advance;
as my main purpose is to get a full grasp of the language itself.

I have no immediate plans for a structured and well maintained repository, 
though you can still find some clues for your own projects.

The repository will hold:

  - Pure JS 
  - JS, JSON and jQuery
  - ReactJS
  - AngularJS
  - Node.js
  
with related HTML and CSS mark-ups.

Suggestions, corrections and/or any type of contribution to the code is welcome.
